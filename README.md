---
output:
  pdf_document: default
  html_document: default
---
# **<span style="color:#00877B"> CST App User Manual </span>**

![GitHub version](https://badge.fury.io/gh/allenzhuaz%2Fcanselect.svg) *(Novemeber 8, 2017)*

### <span style="color:#007265"> About </span>

This interactive application allows you to explore the cans selection based on the combination rules. Change one of the parameters to see what happens.

Check this application from CMS department.

###  <span style="color:#007265">Basic Manufacturing Concept </span>


[TBD]


### <span style="color:#007265"> How To Use This Application </span> 


1. Choose either PQ or Z4C analysis based on the request (for PQ analysis there are two analysis types: `Target` and `Routine`). Upload your raw data by clicking the `Browse` button, 
note the raw data can be any type of file created by Microsoft Excel, such as .csv,.xls,.xlsx,.xlsm, etc.


2. After inputting the parameters in the `Simulation Settings` box, click the `Simulate Blend Cans` button. Then the interactive graphs for the simulated cans combination will be shown in the green box.

3. The raw dataset and simulation output dataset are display under the `Imported Data` and `Output Data` tabs.

4. Users can **download selected rows (only including cans or bulks ID)** and **interactively mark the corresponding points in the plot** by clicking the output data






### <span style="color:#007265"> Author </span>

Center for Mathematical Sciences, Merck.

Yalin Zhu (*Maintainer*) | yalin.zhu@merck.com

Robert Liehr | robert_liehr@merck.com

### <span style="color:#007265"> License </span>

This software is open source and is under the public license [GPL-3.0](http://www.gnu.org/licenses/gpl-3.0.en.html)


### <span style="color:#007265"> Sponsor

This application was developed with [shiny](http://shiny.rstudio.com/) in 
[Rstudio](https://www.rstudio.com/).

Copyright 2017 @ [Center for Mathematical Sciences, Merck.](http://teamspace.merck.com/sites/CMSStatsTrng/default.aspx)

<img src="mercklogo.jpg" width="200">
